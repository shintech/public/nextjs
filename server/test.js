/* eslint-env jest */

const request = require('supertest').agent(_server)

afterAll(async () => {
  await _server.close()
})

describe('SERVER -> GET -> /api...', () => {
  let res

  beforeAll(async () => {
    res = await request.get('/api')
  })

  it('expect res.status to be \'200\'...', () => {
    expect(res.status).toBe(200)
  })

  it('expect res.body to have property \'message\'...', () => {
    expect(res.body).toHaveProperty('message')
  })
})

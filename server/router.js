const express = require('express')
const router = express.Router()

module.exports = function () {
  router.route('/')
    .get((req, res) => {
      res.status(200)
        .json({
          message: 'success'
        })
    })

  return router
}

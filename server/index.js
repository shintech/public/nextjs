const express = require('express')
const bodyParser = require('body-parser')
const compression = require('compression')
const helmet = require('helmet')
const URL = require('url')
const sitemap = require('./routes/sitemap')

module.exports = ({ router }) => {
  const server = express()
    .use('/api', (req, res, next) => {
      const start = Date.now()
      const logger = req.app.get('logger')

      res.on('finish', () => {
        const finish = Date.now() - start

        const url = URL.format({
          host: `${req.get('host')}:${process.env['PORT']}`,
          pathname: req.originalUrl,
          port: process.env['PORT']
        })

        logger.info(`${res.statusCode} - ${req.method} - ${url} - ${finish}ms`)
      })

      next()
    })

    .use(compression())
    .use(bodyParser.urlencoded({ extended: true }))
    .use(bodyParser.json())
    .use(helmet())
    .get('/robots.txt', (req, res) => {
      const robots = (process.env['NODE_ENV'] === 'production') ? `User-agent Googlebot\nAllow: \n\nUser-agent: Mediapartners-Google\nAllow: \n\nUser-agent: *\nDisallow: /static/bundle/*\n\nSitemap: ${req.app.get('options').baseURL}/sitemap.xml` : `User-agent: *\nDisallow: /`

      res.set({
        'Content-Type': 'text/plain'
      })

      res.status(200).send(robots)
    })

    .get('/sitemap.xml', sitemap)

  return server
}

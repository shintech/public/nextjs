const routes = require('next-routes')()

routes.add('home', '/', 'index')
routes.add('about', '/about', 'about')
routes.add('contact', '/contact', 'contact')

module.exports = routes

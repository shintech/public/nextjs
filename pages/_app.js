import React from 'react'
import App from 'next/app'
import { createGlobalStyle } from 'styled-components'
import { PageTransition } from 'next-page-transitions'
import 'isomorphic-unfetch'
import normalize from 'normalize.css/normalize.css'

const TIMEOUT = 500

const GlobalStyle = createGlobalStyle`
  ${normalize}
  
  .page-transition-enter main{
      opacity: 0;
  }

  .page-transition-enter-active main {
    opacity: 1;
    transition: opacity ${TIMEOUT}ms;
  }

  .page-transition-exit main {
    opacity: 1;
  }

  .page-transition-exit-active main {
    opacity: 0;
    transition: opacity ${TIMEOUT}ms;
  }
  
`

class MyApp extends App {
  // static async getInitialProps ({ Component, ctx }) {
  //   const pageProps = Component.getInitialProps
  //     ? await Component.getInitialProps(ctx)
  //     : {}

  //   return { pageProps }
  // }

  // uncommenting getInitalProps in the App Component opts out of Automatic Static Optimization
  // https://err.sh/next.js/opt-out-auto-static-optimization

  render () {
    const { Component, pageProps, router } = this.props

    return (
      <React.Fragment>
        <GlobalStyle />
        <PageTransition timeout={TIMEOUT} classNames='page-transition' key={ router.route }>
          <Component {...pageProps} />
        </PageTransition>
      </React.Fragment>
    )
  }
}

export default MyApp

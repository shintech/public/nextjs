import React from 'react'
import PropTypes from 'prop-types'
import Layout from 'layouts/Main'
import Contact from 'components/Contact'
import fetchJSON from 'lib/fetch'

class Index extends React.Component {
  static propTypes = {
    baseURL: PropTypes.string,
    message: PropTypes.string
  }

  static async getInitialProps ({ store, req, query }) {
    const host = req ? req.headers['host'] : location.host
    const baseURL = `https://${host}`

    const { message } = await fetchJSON(baseURL)

    return {
      baseURL,
      message
    }
  }

  constructor (props) {
    super(props)

    this.state = {
      loaded: false
    }
  }

  componentDidMount () {
    this.setState({ loaded: true })
  }

  render () {
    return (
      <Layout>
        <Contact loaded={this.state.loaded} message={this.props.message} />
      </Layout>
    )
  }
}

export default Index

const http = require('http')
const { Logger } = require('shintech-utils')
const Server = require('./server')
const Router = require('./server/router')
const Cache = require('./lib/cache')
const nextRoutes = require('./routes')
const NAME = process.env['npm_package_name'] || 'app-name'
const VERSION = process.env['npm_package_version'] || '0.0.1'
const PORT = parseInt(process.env['PORT']) || 8000
const NODE_ENV = process.env['NODE_ENV'] || 'development'
const HOST = process.env['HOST'] || '0.0.0.0'

const dev = NODE_ENV !== 'production'
const nextApp = require('next')({ dev })
const handle = nextRoutes.getRequestHandler(nextApp)

nextApp.prepare()
  .then(async () => {
    const logger = new Logger()

    logger.info(`starting -> ${NAME} - version: ${VERSION}...`)

    const router = new Router()
    const app = Server({ router })
    const server = http.Server(app)

    app.set('logger', logger)

    app.use('/api', router)

    if (NODE_ENV === 'production') { Cache(nextApp, app) }

    app.get('*', (req, res) => handle(req, res))

    server.listen(PORT)
      .on('listening', () => logger.info(`listening at ${HOST}:${PORT}...`))
      .on('error', err => logger.error(err.message))
  })

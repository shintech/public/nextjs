/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import Component from 'components/About'

afterAll(async () => {
  await _server.close()
})

describe('COMPONENT -> About -> Server Render...', () => {
  let component = shallow(<Component loaded={false} />)

  it(`expect .svg to have className "server"...`, () => {
    expect(component.hasClass('server')).toBe(true)
  })
})

describe('COMPONENT -> About -> Client Render...', () => {
  let component = shallow(<Component loaded={true} />)

  it(`expect .svg to have className "client"...`, () => {
    expect(component.hasClass('client')).toBe(true)
  })
})

describe('COMPONENT -> About -> snapshot...', () => {
  let component = shallow(<Component loaded={true} />)

  it('expect to render correct properties', () => {
    expect(component).toMatchSnapshot()
  })
})

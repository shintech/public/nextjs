import React from 'react'
import PropTypes from 'prop-types'
import Wrapper from './wrapper'
import SVG from 'public/images/react.svg'

const DemoComponent = ({ loaded, message }) =>
  <Wrapper className={`${(!loaded) ? 'server' : 'client'}`}>
    <SVG className='svg'/>
  </Wrapper>

DemoComponent.propTypes = {
  loaded: PropTypes.bool,
  message: PropTypes.string
}

DemoComponent.defaultProps = {
  loaded: false
}

export default DemoComponent

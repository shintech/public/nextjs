import { withRouter } from 'next/router'
import React, { Children } from 'react'
import routes, { Link } from 'routes'
import PropTypes from 'prop-types'

const ActiveLink = ({ router, children, ...props }) => {
  const child = Children.only(children)
  let href = routes.findAndGetUrls(props.route, props.params).urls.as

  let className = child.props.className || null

  if (router.pathname === href && props.activeClassName) {
    className = `${className !== null ? className : ''} ${props.activeClassName}`.trim()
  }

  const onClick = (className.split(' ').includes('is-active')) ? e => { e.preventDefault() } : null

  delete props.activeClassName

  return <Link {...props}>{React.cloneElement(child, { className, onClick })}</Link>
}

ActiveLink.propTypes = {
  router: PropTypes.object,
  children: PropTypes.object,
  activeClassName: PropTypes.string,
  params: PropTypes.object,
  route: PropTypes.string
}

export default withRouter(ActiveLink)

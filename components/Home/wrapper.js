import styled from 'styled-components'

const Wrapper = styled.div`
  text-align: center;

  opacity: 0;
  transition: opacity 0.5s ease;

  &.client {
    opacity: 1;
    
    svg {
      fill: black;
    }
  }
  
  svg {
    height: 15vh;
    width: 15vw;

    fill: orangered;
    transition: fill 2.5s ease;
  }
`

export default Wrapper

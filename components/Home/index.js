import React from 'react'
import PropTypes from 'prop-types'
import Wrapper from './wrapper'
import SVG from 'public/images/icon.svg'

const HomeComponent = ({ loaded, message }) =>
  <Wrapper className={`${(!loaded) ? 'server' : 'client'}`}>
    <SVG className='svg'/>
  </Wrapper>

HomeComponent.propTypes = {
  loaded: PropTypes.bool,
  message: PropTypes.string
}

HomeComponent.defaultProps = {
  loaded: false
}

export default HomeComponent

/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import Component from 'components/Input'

afterAll(async () => {
  await _server.close()
})

describe('COMPONENT -> Input -> Text Input...', () => {
  let component = shallow(<Component type='text' title='text input' value='' field='title'/>)

  it(`expect label text to be "text input"...`, () => {
    expect(component.find('.label span').text()).toBe('text input')
  })

  it(`expect to render text input...`, () => {
    expect(component.find('input').props().type).toBe('text')
  })
})

describe('COMPONENT -> Input -> Select Input...', () => {
  const array = [ 'a', 'b', 'c' ]

  let component = shallow(<Component type='select' title='select input' value='' field='title' options={array}/>)

  it(`expect label text to be "select input"...`, () => {
    expect(component.find('.label span').text()).toBe('select input')
  })

  it(`expect to render select input...`, () => {
    expect(component.find('select').exists()).toEqual(true)
  })
})

describe('COMPONENT -> Input -> snapshot...', () => {
  let component = shallow(<Component loaded={true} />)

  it('expect to render correct properties', () => {
    expect(component).toMatchSnapshot()
  })
})

import React from 'react'
import PropTypes from 'prop-types'
import Wrapper from './wrapper'

const FormLayout = ({ title, children, handleSubmit }) =>
  <Wrapper className='form-layout' onSubmit={handleSubmit}>
    <h2>{ title }</h2>
    { children }
  </Wrapper>

FormLayout.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
  handleSubmit: PropTypes.func
}

FormLayout.defaultProps = {
  title: 'Form',
  handleSubmit: e => e.preventDefault()
}

export default FormLayout

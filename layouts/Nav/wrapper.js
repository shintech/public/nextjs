import styled from 'styled-components'

const Wrapper = styled.nav`
  position: fixed;
  top: 0;
  width: 100%;

  background-color: ghostwhite;

  .nav-item {
    display: inline-block;
    margin: 1em;
    text-decoration: none;
    color: teal;
    
    &.is-active {
      color: turquoise;
      cursor: not-allowed;
    }
    
    &:hover {
      opacity: 0.8;
    }
  }
`

export default Wrapper

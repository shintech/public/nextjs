import React from 'react'
import PropTypes from 'prop-types'
import Wrapper from './wrapper'
import Link from 'components/Link'

const NavLayout = ({ routes }) =>
  <Wrapper className='navbar'>
    {routes.map((route, v) => {
      return (
        <Link key={v} route={route.page} activeClassName='is-active'>
          <a className='nav-item'>{route.name}</a>
        </Link>
      )
    }
    )}
  </Wrapper>

NavLayout.propTypes = {
  routes: PropTypes.array
}

NavLayout.defaultProps = {
  routes: []
}

export default NavLayout

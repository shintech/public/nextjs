import React from 'react'
import Head from 'next/head'
import PropTypes from 'prop-types'
import Nav from 'layouts/Nav'
import Wrapper from './wrapper'
import { routes } from 'routes'

const MainLayout = ({ title, siteName, description, canonical, children }) =>
  <Wrapper id='main-layout'>
    <Head>
      <title>{ title } | { siteName }</title>
      <meta name='description' content={ description } />
      <link rel='canonical' href={canonical} />
      <link id='favicon' rel='icon' href='/images/icon.ico' />
    </Head>

    <Nav routes={routes}/>
    <main>
      { children }
    </main>
  </Wrapper>

MainLayout.propTypes = {
  title: PropTypes.string,
  siteName: PropTypes.string,
  description: PropTypes.string,
  canonical: PropTypes.string,
  children: PropTypes.node
}

MainLayout.defaultProps = {
  title: 'Default',
  siteName: 'SiteName',
  description: 'Meta Description',
  canonical: 'http://localhost'
}

export default MainLayout

import styled from 'styled-components'

const Wrapper = styled.div`
  font-family: "Times New Roman", Times, serif;

  h1, h2, h3, h4 {font-family: sans-serif;}
  code, pre, tt, kbd {font-family: monospace;}
  p.signature {font-family: cursive;}
  .tagline {font-family: "Lucida Console", Monaco, monospace;}

  background-color: #6d695c;
  background-image:
  repeating-linear-gradient(120deg, rgba(255,255,255,.1), rgba(255,255,255,.1) 1px, transparent 1px, transparent 60px),
  repeating-linear-gradient(60deg, rgba(255,255,255,.1), rgba(255,255,255,.1) 1px, transparent 1px, transparent 60px),
  linear-gradient(60deg, rgba(0,0,0,.1) 25%, transparent 25%, transparent 75%, rgba(0,0,0,.1) 75%, rgba(0,0,0,.1)),
  linear-gradient(120deg, rgba(0,0,0,.1) 25%, transparent 25%, transparent 75%, rgba(0,0,0,.1) 75%, rgba(0,0,0,.1));
  background-size: 70px 120px;

  height: 100vh;

  main {
    width: 96vw;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`

export default Wrapper

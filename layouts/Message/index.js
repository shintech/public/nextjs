import React from 'react'
import PropTypes from 'prop-types'
import Wrapper from './wrapper'

const MessageComponent = ({ title, children }) =>
  <Wrapper className='message'>
    <h3>{ title }</h3>
    { children }
  </Wrapper>

MessageComponent.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node
}

MessageComponent.defaultProps = {
  title: null,
  message: null
}

export default MessageComponent

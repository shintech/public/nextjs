import styled from 'styled-components'

const Wrapper = styled.div`
  background-color: ghostwhite;
  padding: 0.5em 1em;
  margin: 1em 0;
  border: 1px solid black;
  border-radius: 0.5em;
  text-align: center;

  h3 {
    text-transform: capitalize;
  }
`

export default Wrapper

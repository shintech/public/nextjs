const mongoose = require('mongoose')
const faker = require('faker')
const slugify = require('slugify')
const User = require('./server/models/User')
const Post = require('./server/models/Post')
const Page = require('./server/models/Page')
const Feed = require('./server/models/Feed')
const Option = require('./server/models/Option')
const pkg = require('./package.json')

mongoose.Promise = require('bluebird')
const markdown = `
1. Description
2. Code Example
3. Paragraph

***

Delectus aut ratione qui. Eum sapiente ipsum est ipsum ut. Quaerat esse qui exercitationem qui dignissimos expedita dolorem et. Et nobis sit nulla ipsa corporis voluptatem sint.

#### Code Example

~~~js
import Post from 'components/Post'
import PropTypes from 'prop-types'
import Wrapper from './wrapper'
import theme from './theme'

function posts () {
  return [
   {
    title: 'test',
    content: 'test',
    author: 'test'
   }
  ]
}

const Title = () =>
  <Wrapper>
    <div className='post-list'>
      { posts().data.map((post, n) => {
        return <Post key={n} post={post} />
      })}
    </div>
    
    <div className='contact-info'>
      <h3>Contact</h3>
      <div className='address'>
        Tulsa, OK, 74136
      </div>
      <div className='phone'>918-555-5555</div>
      <div className='email'><a href='#'>email@example.org</a></div>
    </div>
  </Wrapper>

Title.propTypes = {
  title: PropTypes.string.isRequired,
  fontSize: PropTypes.string,
  colors: PropTypes.array
}

export default Title
~~~
`

const environment = process.env['NODE_ENV'] || 'development'
const baseURL = process.env['BASE_URL'] || 'http://localhost/'
const staticAssets = process.env['STATIC_ASSETS'] || 'http://localhost/'
const reCaptchaSiteKey = process.env['RECAPTCHA_SITE_KEY'] || 'captcha'
const connectionString = `${process.env['MONGO_URI']}/${pkg.name}_${environment}`

const posts = new Array(8).fill(null).map(() => ({
  _id: new mongoose.Types.ObjectId(),
  title: faker.company.catchPhrase(),
  slug: slugify(faker.company.catchPhrase(), { lower: true }),
  description: faker.lorem.paragraph(),
  content: markdown,
  image: '/images/blogPost.jpg'
}))

const createPages = () => {
  const home = new Page()

  const homeData = [
    {
      title: faker.company.bsNoun(),
      content: faker.lorem.paragraph(),
      image: '/images/poop.svg'
    },

    {
      title: faker.company.bsNoun(),
      content: faker.lorem.paragraph(),
      image: '/images/poop.svg'
    },

    {
      title: faker.company.bsNoun(),
      content: faker.lorem.paragraph(),
      image: '/images/poop.svg'
    },

    {
      title: faker.company.bsNoun(),
      content: faker.lorem.paragraph(),
      image: '/images/poop.svg'
    }
  ]

  home.set({
    route: '/',
    name: 'home',
    title: 'index.js',
    favicon: `/images/icon.svg`,

    header: {
      image: `/images/indexHeader.jpg`,
      height: '100vh'
    },

    content: [ faker.lorem.paragraphs() ],
    data: homeData
  })

  home.save(function (err, docs) {
    if (err) {
      throw new Error(err)
    } else {
      console.log('Created Home page data -> Success...')
      mongoose.connection.close()
    }
  })

  const about = new Page()

  const aboutData = new Array(6).fill(null).map(() => ({
    employeeName: `${faker.name.firstName()} ${faker.name.lastName()}`,
    employeeBio: faker.lorem.paragraph(),
    jobDescription: faker.name.jobTitle(),
    image: '/images/poop.svg'
  }))

  about.set({
    route: '/about',
    name: 'about',
    title: 'about.js',
    favicon: `/images/icon.svg`,

    header: {
      image: `/images/pageHeader.jpg`,
      height: '32vh'
    },

    content: [ faker.lorem.paragraphs(), faker.lorem.paragraphs() ],
    data: aboutData
  })

  about.save(function (err, docs) {
    if (err) {
      throw new Error(err)
    } else {
      console.log('Created About page data -> Success...')
      mongoose.connection.close()
    }
  })

  const contact = new Page()

  contact.set({
    route: '/contact',
    name: 'contact',
    title: 'contact.js',
    favicon: `/images/icon.svg`,

    header: {
      image: `/images/pageHeader.jpg`,
      height: '32vh'
    },

    content: [ faker.lorem.paragraphs() ]
  })

  contact.save(function (err, docs) {
    if (err) {
      throw new Error(err)
    } else {
      console.log('Created Contact page data -> Success...')
      mongoose.connection.close()
    }
  })
}

const createFeeds = () => {
  const feed = new Feed()

  feed.set({
    posts: posts.map((f, v) => f._id),
    route: '/posts',
    name: 'blog',
    title: 'blog.js',
    favicon: `/images/icon.svg`,
    header: {
      image: `/images/blogHeader.jpg`,
      height: '32vh'
    },

    content: [ faker.lorem.paragraphs() ],

    ads: {
      feed: {
        format: 'test',
        layoutKey: 'layoutKey',
        client: 'client',
        slot: 'slot'
      }
    }
  })

  feed.save(function (err, docs) {
    if (err) {
      throw new Error(err)
    } else {
      console.log('Created Feed data -> Success...')
      mongoose.connection.close()
    }
  })
}

const createPosts = () => {
  Post.collection.insertMany(posts, function (err, docs) {
    if (err) {
      throw new Error(err)
    } else {
      console.log('Created Posts data -> Success...')
      mongoose.connection.close()
    }
  })
}

const createOptions = () => {
  const options = [
    {
      name: 'siteName',
      description: 'The name of the company -> example.com',
      value: faker.company.companyName()
    },

    {
      name: 'baseURL',
      description: 'The URL for the site -> https://www.domain.com/',
      value: baseURL
    },

    {
      name: 'staticAssets',
      description: 'The URL for static assets -> https://static.domain.com/',
      value: staticAssets
    },

    {
      name: 'smtpSender',
      description: 'The user that will be authenticating with SMTP.',
      value: process.env['SMTP_USER']
    },

    {
      name: 'smtpBCC',
      description: 'The user that will be authenticating with SMTP./',
      value: process.env['SMTP_BCC']
    },

    {
      name: 'smtpHost',
      description: 'The SMTP host',
      value: process.env['SMTP_HOST']
    },

    {
      name: 'smtpPort',
      description: 'The SMTP port.',
      value: process.env['SMTP_PORT']
    },

    {
      name: 'smtpPass',
      description: 'The user that will be authenticating with SMTP./',
      value: process.env['SMTP_PASS']
    },

    {
      name: 'reCaptchaSiteKey',
      description: 'site key for Google reCaptcha',
      value: reCaptchaSiteKey
    },

    {
      name: 'adSenseId',
      description: 'Google AdSense Id',
      value: 'adsenseId'
    },

    {
      name: 'facebookProfile',
      description: 'The URL for the Facebook profile',
      value: '#'
    },

    {
      name: 'linkedinProfile',
      description: 'The URL for the Linkedin profile',
      value: '#'
    },

    {
      name: 'twitterProfile',
      description: 'The URL for the Twitter account',
      value: '#'
    },

    {
      name: 'logDirectory',
      description: 'The directory where the logs are stored - no leading "/" -> log/sub',
      value: 'log'
    },

    {
      name: 'siteDescription',
      description: 'A paragraph of corporate bullshit',
      value: faker.lorem.paragraph()
    },

    {
      name: 'siteTag',
      description: 'One sentence of corporate bullshit',
      value: faker.company.bs()
    },

    {
      name: 'contactPhone',
      description: 'Contact phone number',
      value: faker.phone.phoneNumber()
    },

    {
      name: 'contactEmail',
      description: 'Contact email',
      value: faker.internet.exampleEmail()
    },

    {
      name: 'primaryColor',
      description: 'Primary color for theme',
      value: 'darkslategrey'
    },

    {
      name: 'secondaryColor',
      description: 'Secondary color for theme',
      value: 'red'
    },

    {
      name: 'textColor',
      description: 'Text color for theme',
      value: 'black'
    },

    {
      name: 'accentColor',
      description: 'Accent color for theme',
      value: 'orange'
    },

    {
      name: 'backgroundColor',
      description: 'Color for theme background - box',
      value: 'mintcream'
    },

    {
      name: 'secondaryBackgroundColor',
      description: 'Secondary color for theme background - box',
      value: 'lightyellow'
    },

    {
      name: 'activeColor',
      description: 'Active color for theme',
      value: 'teal'
    },

    {
      name: 'errorColor',
      description: 'Error color for theme',
      value: 'red'
    }
  ]

  Option.collection.insertMany(options, function (err, docs) {
    if (err) {
      throw new Error(err)
    } else {
      console.log('Created Options data -> Success...')
      mongoose.connection.close()
    }
  })
}

const createUsers = () => {
  const user = new User()

  const users = new Array(1).fill(null).map(() => ({
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    username: process.env['ADMIN_USER'] || 'username',
    password: user.generateHash(process.env['PASSWORD'] || 'password')
  }))

  User.collection.insertMany(users, function (err, docs) {
    if (err) {
      throw new Error(err)
    } else {
      console.log('Created Users data -> Success...')
      mongoose.connection.close()
    }
  })
}

const dbOptions = {
  useNewUrlParser: true,
  connectTimeoutMS: 10000,
  user: process.env['MONGO_USER'],
  pass: process.env['MONGO_PASSWORD']
}

mongoose.connect(`${connectionString}?authSource=admin`, dbOptions, function (err, res) {
  if (err) { throw new Error(err) }

  console.log('connected to database: ' + connectionString)

  mongoose.connection.db.dropDatabase(() => {
    createPages()
    createFeeds()
    createPosts()
    createOptions()
    createUsers()
  })
})
